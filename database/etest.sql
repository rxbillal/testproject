-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 06, 2023 at 07:25 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `etest`
--

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`, `email`, `logo`, `website`, `created_at`, `updated_at`) VALUES
(5, 'Update', 'hzbillal303@gmail.com', 'logo/TASQMiDEZQeqlRXgHpPFjTgCgdGTS44ttppMTeWb.png', 'https://www.tipstunebd.com', '2023-02-06 11:32:35', '2023-02-06 11:32:54'),
(6, 'Chloe Gamble', 'mohatedomi@mailinator.com', 'logo/1CkugrdHCPQKL7q2qnxrmpSVYhD1icQkf7j60S8D.png', 'https://www.vykomizykeseda.biz', '2023-02-06 12:20:09', '2023-02-06 12:20:09'),
(7, 'Melodie Palmer', 'hunavube@mailinator.com', 'logo/3HnyouVeEvSWS48jA3Us6phX5HAMl9iYEw66FJfI.jpg', 'https://www.gusypikakex.mobi', '2023-02-06 12:20:19', '2023-02-06 12:20:19'),
(8, 'Leilani Sexton', 'zofi@mailinator.com', 'logo/AyADSbUCvNatXDcDtS1272pzOAHfOn8Fx0hepwMJ.jpg', 'https://www.tonagyf.com', '2023-02-06 12:20:35', '2023-02-06 12:20:35'),
(9, 'Oscar Carey', 'pafexi@mailinator.com', 'logo/B3EqoXJKNxevl46THJ1UldsMdXWAFTXFK0W4B8Lu.png', 'https://www.fotucinyjukuvi.in', '2023-02-06 12:20:47', '2023-02-06 12:20:47'),
(10, 'Cade Reid', 'qasytygyg@mailinator.com', 'logo/Cj1qkUxE0D04XlFG9NX27fsaB5yvgtk5grzDslpl.jpg', 'https://www.cewyqeqimy.info', '2023-02-06 12:20:58', '2023-02-06 12:20:58'),
(11, 'Hedley Flynn', 'wonyhuka@mailinator.com', 'logo/H4TxKUMWp37GvFM0u1C7YUaXzo9IvIQautn6aiOf.png', 'https://www.xohowahedafu.org.uk', '2023-02-06 12:21:11', '2023-02-06 12:21:11'),
(12, 'Rebekah Acevedo', 'wugacup@mailinator.com', 'logo/e5NdyfaZjEQ4vdSTgbinFt5p8XzwVSl4dY3BHIFq.jpg', 'https://www.zyfanarycumic.co', '2023-02-06 12:21:25', '2023-02-06 12:21:25'),
(13, 'Germane Potter', 'quruw@mailinator.com', 'logo/xBoa57e0Gq7Dw9SdUQEwvlOlQ6fIJ6Qm8Yf6HXcY.png', 'https://www.fumahehupocuba.com.au', '2023-02-06 12:21:43', '2023-02-06 12:21:43'),
(14, 'Stacy Holder', 'ladatuz@mailinator.com', 'logo/xVDlr0udOVmzVhQo8uMwZJHnnGaGAfIKB2xlT8K0.png', 'https://www.kafiwiq.com', '2023-02-06 12:21:56', '2023-02-06 12:21:56'),
(15, 'Rajah Moon', 'qahoce@mailinator.com', 'logo/EQRgPAYkFcF7D4VOiLAvWEtzHId43eZrFqjcc75g.png', 'https://www.jimegyfazevumym.com.au', '2023-02-06 12:22:08', '2023-02-06 12:22:08');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `company_id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES
(3, 4, 'Your', 'Name', 'aci.emplyee@gmail.com', 1213456789, '2023-02-06 06:49:59', '2023-02-06 06:49:59'),
(4, 4, 'ghjgjhgf', 'gfjgjgjgj', 'gjgjg@gdsgd.com', 1457587462, '2023-02-06 07:44:42', '2023-02-06 07:44:42'),
(5, 5, 'ujhrusr', 'yrfyr', 'hzbillal303@gmail.com', 123458245, '2023-02-06 08:42:05', '2023-02-06 08:42:05'),
(6, 5, 'updatte 1', 'jjjj', '123@gmail.com', 123458245, '2023-02-06 08:53:58', '2023-02-06 08:53:58'),
(7, 5, 'update 2', 'yrfyr', 'hzbillal303@gmail.com', 123458245, '2023-02-06 08:54:15', '2023-02-06 08:54:15'),
(8, 5, 'Update 3', 'yrfyr', 'hzbillal303@gmail.com', 123458245, '2023-02-06 08:54:26', '2023-02-06 08:54:26'),
(9, 5, 'Update 3', 'yrfyr', 'hzbillal303@gmail.com', 123458245, '2023-02-06 11:50:17', '2023-02-06 11:50:17'),
(10, 3, 'Helen', 'Juarez', 'wowupibuda@mailinator.com', 77, '2023-02-06 12:17:56', '2023-02-06 12:17:56'),
(11, 1, 'Judah', 'Bean', 'vidojovy@mailinator.com', 23, '2023-02-06 12:18:03', '2023-02-06 12:18:03'),
(12, 5, 'Celeste', 'Nolan', 'kufalo@mailinator.com', 75, '2023-02-06 12:18:08', '2023-02-06 12:18:08'),
(13, 4, 'Zoe', 'Olsen', 'xibemi@mailinator.com', 42, '2023-02-06 12:18:20', '2023-02-06 12:18:20'),
(14, 4, 'Talon', 'Stanton', 'dususit@mailinator.com', 81, '2023-02-06 12:18:27', '2023-02-06 12:18:27');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_02_05_105048_create_employees_table', 1),
(6, '2023_02_05_105251_create_companies_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@admin.com', NULL, '$2y$10$53pDywaJJ.0gFhjhn/PxEeW4UVWg0e/Zo.rCoEtkTUZ4OloThl1tu', NULL, '2023-02-06 04:44:22', '2023-02-06 04:44:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
