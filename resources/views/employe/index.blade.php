<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight d-flex justify-content-evenly">
            <a href="{{ url('company') }}"><button class="btn btn-outline-dark ">Companyes</button></a>
            ||--||
            <a href="{{ url('employe') }}"><button class="btn btn-outline-dark">Employee</button></a>
        </h2>
    </x-slot>

    <div class="container mt-5 ">
        <div class="float-end">
            <h2 class="btn btn-success" data-bs-toggle="modal" data-bs-target="#exampleModal">
                Add Employe
            </h2>
        </div>
    </div>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">First Name</th>
                            <th scope="col">Last Name</th>
                            <th scope="col">Company Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Phone</th>
                            <th class="float-end" scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($employes as $employe)
                            <tr>
                                <th scope="row">{{ $employes->firstItem() + $loop->index }}</th>
                                <td>{{ $employe->first_name }}</td>
                                <td>{{ $employe->last_name }}</td>
                                <td>{{ $employe->company->name?? "None" }}</td>
                                <td>{{ $employe->email }}</td>
                                <td>{{ $employe->phone }}</td>
                                <td class="float-end">
                                    <a href="{{ route('employe.edit', [$employe->id]) }}"
                                        class="btn btn-primary a-btn-slide-text">
                                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                        <span><strong>Edit</strong></span>
                                    </a>
                                    <form class="btn btn-primary a-btn-slide-text"
                                        action="{{ route('employe.destroy', $employe->id) }}" method="post">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit"
                                            onclick="return confirm('Are you sure to delete this Employe?')"><span><strong>Delete</strong></span></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $employes->withQueryString()->links() !!}
            </div>
        </div>
    </div>
    </div>


    <!-- Button trigger modal -->


    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Employe Details</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('employe.store') }}" method="POST">
                        @csrf
                        <div class="mb-1">
                            <label for="first_name" class="col-form-label">First Name:</label>
                            <input type="text" name="first_name" class="form-control" id="first_name">
                        </div>
                        <div class="mb-1">
                            <label for="last_name" class="col-form-label">Last Name::</label>
                            <input type="text" name="last_name" class="form-control" id="last_name">
                        </div>
                        <div class="mb-1">
                            <label for="last_name" class="col-form-label">Company:</label>
                            <select class="form-control" name="company_id" id="company_id">
                                @foreach ($companys as $company)
                                    <option value="{{ $company->id }}">{{ $company->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-1">
                            <label for="Email" class="col-form-label">Email:</label>
                            <input type="email" name="Email" class="form-control" id="Email">
                        </div>
                        <div class="mb-1">
                            <label for="Phone" class="col-form-label">Phone:</label>
                            <input type="number" name="Phone" class="form-control" id="Phone">
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary text-dark" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary text-dark">Save Data</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    </div>

</x-app-layout>
