<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight d-flex justify-content-evenly">
            <a href="{{ url('company') }}"><button class="btn btn-outline-dark ">Companyes</button></a>
            ||--||
            <a href="{{ url('employe') }}"><button class="btn btn-outline-dark">Employee</button></a>
        </h2>
    </x-slot>

    <div class="container mt-4 ">
        <div class="text-center">
            <b>Update Employe Information</b>
        </div>
    </div>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                <form action="{{ route('employe.update', $employe->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="mb-1">
                        <label for="first_name" class="col-form-label">First Name:</label>
                        <input type="text" name="first_name" class="form-control" value="{{ $employe->first_name }}">
                    </div>
                    <div class="mb-1">
                        <label for="last_name" class="col-form-label">Last Name::</label>
                        <input type="text" name="last_name" class="form-control" value="{{ $employe->last_name }}">
                    </div>
                    <div class="mb-1">
                        <label for="last_name" class="col-form-label">Company:</label>
                        <select class="form-control" name="company_id" id="company_id">
                            @foreach ($companys as $company)
                                <option @if ($employe->company_id == $company->id) selected @endif value="{{ $company->id }}">
                                    {{ $company->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-1">
                        <label for="Email" class="col-form-label">Email:</label>
                        <input type="email" name="email" class="form-control" value="{{ $employe->email }}">
                    </div>
                    <div class="mb-1">
                        <label for="Phone" class="col-form-label">Phone:</label>
                        <input type="number" name="phone" class="form-control" value="{{ $employe->phone }}">
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary text-dark">Update Data</button>
            </div>
            </form>
        </div>
    </div>
    </div>
    </div>


    <!-- Button trigger modal -->
    </div>

</x-app-layout>
