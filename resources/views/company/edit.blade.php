<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight d-flex justify-content-evenly">
            <a href="{{ url('company') }}"><button class="btn btn-outline-dark ">Companyes</button></a>
            ||--||
            <a href="{{ url('employe') }}"><button class="btn btn-outline-dark">Employee</button></a>
        </h2>
    </x-slot>

    <div class="container mt-4 ">
        <div class="text-center">
            <b>Update Company Information</b>
        </div>
    </div>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                <form action="{{ route('company.update', $company->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="mb-1">
                        <label for="name" class="col-form-label">Company Name:</label>
                        <input type="text" name="name" class="form-control" value="{{ $company->name }}">
                    </div>
                    <div class="mb-1">
                        <label for="email" class="col-form-label">E-Mail:</label>
                        <input type="text" name="email" class="form-control" value="{{ $company->email }}">
                    </div>
                    <div class="mb-1">
                        <img src="{{ asset('storage/'.$company->logo) }}" width="100px;" height="100px;" alt="OldLogo">
                        <label for="logo" class="col-form-label">Logo:</label>
                        <input type="file" name="logo" class="form-control">
                    </div>
                    <div class="mb-1">
                        <label for="website" class="col-form-label">Website:</label>
                        <input type="text" name="website" class="form-control" value="{{ $company->website }}">
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary text-dark">Update Data</button>
            </div>
            </form>
        </div>
    </div>
    </div>
    </div>


    <!-- Button trigger modal -->

    </div>

</x-app-layout>
