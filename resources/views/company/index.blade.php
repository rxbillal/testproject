<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight d-flex justify-content-evenly">
            <a href="{{ url('company') }}"><button class="btn btn-outline-dark ">Companyes</button></a>
            ||--||
            <a href="{{ url('employe') }}"><button class="btn btn-outline-dark">Employee</button></a>
        </h2>
    </x-slot>

    <div class="container mt-5 ">
        <div class="float-end">
            <h2 class="btn btn-success" data-bs-toggle="modal" data-bs-target="#exampleModal">
                Add Company
            </h2>
        </div>
    </div>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Logo</th>
                            <th scope="col">Website</th>
                            <th class="float-end" scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($companys as $company)
                            <tr>
                                <th scope="row">{{ $companys->firstItem() + $loop->index }}</th>
                                <td>{{ $company->name }}</td>
                                <td>{{ $company->email }}</td>
                                <td><img src="{{ asset('storage/'.$company->logo) }}" width="50px;" height="50px;" alt="">
                                </td>
                                <td>{{ $company->website }}</td>
                                <td class="float-end">
                                    <a href="{{ route('company.edit', $company->id) }}"
                                        class="btn btn-primary a-btn-slide-text">
                                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                        <span><strong>Edit</strong></span>
                                    </a>
                                    <form class="btn btn-primary a-btn-slide-text"
                                        action="{{ route('company.destroy', $company->id) }}" method="post">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit"><span><strong>Delete</strong></span></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $companys->withQueryString()->links() !!}
            </div>
        </div>
    </div>
    </div>


    <!-- Button trigger modal -->


    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Company Details</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('company.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                            <label for="name" class="col-form-label">Name:</label>
                            <input type="text" name="name" class="form-control" id="name">
                        </div>
                        <div class="mb-3">
                            <label for="email" class="col-form-label">Email:</label>
                            <input type="text" name="email" class="form-control" id="email">
                        </div>
                        <div class="mb-3">
                            <label for="logo" class="col-form-label">logo:</label>
                            <input type="file" name="logo" class="form-control" id="logo">
                        </div>
                        <div class="mb-3">
                            <label for="website" class="col-form-label">Website:</label>
                            <input type="text" name="website" class="form-control" id="website">
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary text-dark" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary text-dark">Save Data</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    </div>

</x-app-layout>
