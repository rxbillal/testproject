<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight d-flex justify-content-evenly">
           <a href="{{ url('company') }}"><button class="btn btn-info ">Companyes</button></a>
           ||--||
           <a href="{{ url('employe') }}"><button class="btn btn-info">Employee</button></a>
        </h2>
    </x-slot>

    <div class="mt-5 container">
        <div class="row ">
            <div class="col-sm-6">
              <div class="card">
                <div class="card-body">
                  <h5 class="card-title"><h1>Total Company <span class="badge bg-primary">{{ App\Models\Company::count() }}</span></h1></h5>
                    <a href="{{ url('company') }}" class="btn btn-primary mt-2">Go Company</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="card">
                <div class="card-body">
                  <h5 class="card-title"><h1>Total Employee <span class="badge bg-success">{{ App\Models\Employee::count() }}</span></h1></h5>
                    <a href="{{ url('employe') }}" class="btn btn-primary mt-2">Go Employee</a>
                </div>
              </div>
            </div>
          </div>
    </div>

</x-app-layout>
