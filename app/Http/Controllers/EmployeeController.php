<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Employee;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\Return_;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\EmployeRequest;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employes = Employee::latest()->paginate(10);
        $companys = Company::latest()->get();
        return view('employe.index',compact('employes','companys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeRequest $request)
    {
        // Validate
        $validator = $request->validated();

        // Store Data
        Employee::create([
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'company_id'=>$request->company_id,
            'email'=>$request->Email,
            'phone'=>$request->Phone,
        ]);
        return Redirect()->back()->with('success','Employee Added Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employe)
    {
        $companys = Company::latest()->get();
        return view('employe.edit',compact('companys','employe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $employee->fill($request->post())->save();
        return Redirect()->route('employe.index')->with('success','Employee Data Update Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employe)
    {
        $employe->delete();
        return Redirect()->back()->with('success','Delete Data Success');
    }
}
