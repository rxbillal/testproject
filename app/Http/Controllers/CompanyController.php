<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Requests\CompanyRequest;


class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companys = Company::latest()->paginate(10);
        return view('company.index',compact('companys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {
        // Validate
        $validator = $request->validated();

           $name = $request->file('logo')->getClientOriginalExtension();
           $image_path = $request->file('logo')->store('logo', 'public');
           Company::create([
                'name'=>$request->name,
                'email'=>$request->email,
                'logo'=>$request-> $name.$image_path,
                'website'=>$request->website,
            ]);

        return Redirect()->back()->with('success','Company Added Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('company.edit',compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyRequest $request, Company $company)
    {

        $company->update([
            'name'=>$request->name,
            'email'=>$request->email,
            'website'=>$request->website,
        ]);

        if ($request->has('logo') ) {
            unlink('storage/'.$company->logo);

            $name = $request->file('logo')->getClientOriginalExtension();
            $image_path = $request->file('logo')->store('logo', 'public');

            $company->update(['logo'=>$request->$name.$image_path]);

             return Redirect()->route('company.index')->with('success','image successfully Updated');
         }
        return Redirect()->route('company.index')->with('success','Company Data Update Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)

    {
       $company->delete();
       return Redirect()->back()->with('success','Delete Data Success');
    }
}
