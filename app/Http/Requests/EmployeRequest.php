<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
         return [
            'first_name'=> 'required|max:100',
            'last_name'=> 'required|max:100',
            'company_id'=> 'required',
        ];
    }

    public function messages()

    {
        return [
            'first_name.required' =>'Please Enter First Name!',
            'last_name.required' =>'Please Enter Last Name!',
            'company_id.required' =>'Please Select Your Company!',
        ];

    }
}
