<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
         return [
            'name'=> 'required',
            'email'=> 'required',
            'logo'=> 'nullable|mimes:jpg,png,jpeg,gif,svg|max:2048|dimensions:min_width=100,min_height=100',
            'website'=> 'required',
        ];
    }

    public function messages()

    {
        return [
            'name.required' =>'Please Enter Company Name!',
            'email.required' =>'Please Enter Company Name!',
            'logo.nullable' =>'Select Logo Size Min 100x100!',
            'website.required' =>'Please Select Your Company!',
        ];

    }
}
