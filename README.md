<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>



# Laravel 9 CURD System


## Login page
https://tipstunebd.com/demo/public/



## How to run the code
- git clone https://gitlab.com/rxbillal/testproject.git
- cd testproject
- cp .env.example `.env`
- open .env and update DB_DATABASE (database details)
- run : `composer update`
- run : `php artisan key:generate`
- run : `php artisan migrate:fresh --seed`
- run : `php artisan serve`

- Best of luck 


## Credentials
- #### Admin 
- email: admin@admin.com
- password : password